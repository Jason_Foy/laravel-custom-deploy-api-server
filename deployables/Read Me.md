<h3>Following System Requirements</h3>
<ul>
    <li>PHP 7.0></li>
    <ul>
    <li>php7.0-mbstring</li>
    <li>php7.0-xml</li>
    <li>libapache2-mod-php</li>
    <li>php7.0-mysql</li>
    </ul>
    <li>zip</li>
    <li>Apache 2.4</li>
    <li>Git</li>
    <li>Composer</li>
</ul>

<h3>Deployment</h3>
<ol>
    <li>Run "sudo clone git <repo path> /var/www/site"</li>
    <li>Run "sudo chmod -R 775 /var/www/site/deployables"</li>
    <li>Run "sudo own -R root:<username> /var/www/site/deployables"</li>
    <li>Run "sudo ./deploy.sh"</li>
    <li>Select Update</li>
    <li>Run "sudo ./deploy.sh"</li>
    <li>Select Deploy</li>
    <li>Select "Deploy Updated Build"</li>
    <li>Enjoy</li>
</ol>

<h3>Server Setup SQL Proxy<h3>
View ./systemd readme file