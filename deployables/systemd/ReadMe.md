<h1>Install Proxy Service</h1>

<ol>
    <li>cp /var/www/site/deployable/systemd/googleproxy.service /etc/systemd/system</li>
    <li>sudo passwd (create root password)</li>
    <li> su (login)</li>
    <li> systemctl enable googleproxy.service</li>
    <li> /var/www/site/deployables/systemd/./install_google_proxy</li>
    <li> reboot </li>
</ol>

<h1>Test Service</h1>

run <b> mysql -u test-server-acc -p -S /cloudsql/sharp-sandbox-155516:us-central1:instance-2 </b>


<h1>Notes</h1>
<ul>
    <li>Compute engine must have API enabled</li>
    <li>Mysql Server Must have the flag "log_bin_trust_function_creators" set to true</li>
</ul>


https://cloud.google.com/sql/docs/mysql/flags