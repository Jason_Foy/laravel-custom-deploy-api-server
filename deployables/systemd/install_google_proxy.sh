#!/usr/bin/env bash

wget https://dl.google.com/cloudsql/cloud_sql_proxy.linux.amd64
mv cloud_sql_proxy.linux.amd64 /cloud_sql_proxy
chmod +x /cloud_sql_proxy

mkdir /cloudsql
chmod 777 /cloudsql