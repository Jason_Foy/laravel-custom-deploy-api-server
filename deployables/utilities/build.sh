#!/usr/bin/env bash


if [ ! -d apache ];
    then
        printf "Missing Apache Conf Files, try and update";
        exit;
    fi

if [ ! -d  .temp/htdocs ];
    then
        printf "Missing Project Files in .temp";
        exit;
    fi

if [ ! -d  laravel ];
    then
        printf "Missing Project Files in laravel";
        exit;
    fi

#
#   Setup Laravel Env
#
while true; do
    read -p "What Env Would you like to deploy?

Local(l)
Production(p)
Test(t)
:" response
    case $response in
        [l]* ) todo=1; break;;
        [p]* ) todo=2; break;;
        [t]* ) todo=3; break;;
        * ) echo "Please answer Local(l), Production(p) or Test(t).";;
    esac
done


#
# Setup Maintenance Site
#
./utilities/maintenance.sh

#
#   Update Files
#
printf "Update Project Files With Latest Update Build\n";
rm -Rf /var/www/site/htdocs
cp -Rf $PWD/.temp/htdocs /var/www/site/htdocs
printf "Completed\n";
printf "_____________\n";

#
#   Run Composer
#
printf "Update Composer\n";
cd /var/www/site/htdocs
composer update
printf "Completed\n";
printf "_____________\n";

cd /var/www/site/deployables
#
#   Update Apache
#
printf "Update Main Apache Configuration\n";
rm -f /etc/apache/sites-available/main.conf
rm -f /etc/apache/sites-available/main-ssl.conf
a2dissite default-ssl.conf
a2dissite 000-default.conf
rm -f /etc/apache/sites-available/default-ssl.conf
rm -f /etc/apache/sites-available/000-default.conf
cp $PWD/apache/main.conf /etc/apache2/sites-available
cp $PWD/apache/main-ssl.conf /etc/apache2/sites-available

a2enmod rewrite
printf "Completed\n";
printf "_____________\n";


printf "Disable Maintenance Enable Main Configurations\n";
a2dissite maintenance.conf
a2dissite maintenance-ssl.conf
a2ensite main.conf
a2ensite main-ssl.conf
printf "Completed\n";
printf "_____________\n";

#
#   Copy Env File
#
if [ $todo -eq 1 ];
    then
        cp -Rf $PWD/laravel/.env-dev /var/www/site/htdocs/.env
        printf "Env Set to Development\m";
    fi

if [ $todo -eq 2 ];
    then
        cp -Rf $PWD/laravel/.env-prod /var/www/site/htdocs/.env
        printf "Env Set to Production\m";
    fi

if [ $todo -eq 3 ];
    then
        cp -Rf $PWD/laravel/.env-test /var/www/site/htdocs/.env
        printf "Env Set to Test\m";
    fi
printf "Completed\n";
printf "_____________\n";

#
#   File Permissions
#
printf "Changing Files Permissions\n";
chown -Rf root:www-data /var/www/site/htdocs
cd /var/www/site/htdocs/
sudo chgrp -R www-data storage bootstrap/cache
sudo chmod -R ug+rwx storage bootstrap/cache
printf "Completed\n";
printf "_____________\n";

#
#   Restart Apache
#
printf "Update Logs and Restart Server\n";
echo "Updated On "$(date --utc +%FT%T.%3NZ)>> $PWD/deployed.log
service apache2 restart
printf "Completed\n";
printf "_____________\n";

