#!/usr/bin/env bash
printf "Disable All Apache Sites\n";
a2dissite main.conf
a2dissite main-ssl.conf
a2dissite maintenance.conf
a2dissite maintenance-ssl.conf
printf "Completed\n";
printf "_____________\n";


printf "Update Maintenance Configuration\n";
rm -f /etc/apache/sites-available/maintenance.conf
rm -f /etc/apache/sites-available/maintenance-ssl.conf
cp $PWD/apache/maintenance.conf /etc/apache2/sites-available
cp $PWD/apache/maintenance-ssl.conf /etc/apache2/sites-available
printf "Completed\n";
printf "_____________\n";

printf "Enable New Configuration\n";
a2ensite maintenance.conf
a2ensite maintenance-ssl.conf
printf "Completed\n";
printf "_____________\n";

printf "Restart Apache Server\n";
service apache2 restart
printf "Completed\n";
printf "_____________\n";