#!/usr/bin/env bash
printf "_____________\n\n";
printf "Updating Deployable Scripts\n";
rm -f $PWD/deploy
cp -f $PWD/.temp/deployables/deploy.sh $PWD
printf "Completed\n";
printf "_____________\n";

printf "Updating Utilities Scripts\n";
rm -Rf $PWD/utilities
cp -Rf $PWD/.temp/deployables/utilities $PWD
printf "Completed\n";
printf "_____________\n";

printf "Updating Apache Scripts\n";
rm -Rf $PWD/apache
cp -Rf $PWD/.temp/deployables/apache $PWD
printf "Completed\n";
printf "_____________\n";

printf "Updating Laravel Env Page Script\n";
rm -fR $PWD/signs
cp -fR $PWD/.temp/deployables/signs $PWD
printf "Completed\n";
printf "_________\n";

printf "Updating Signs Page Script\n";
rm -fR $PWD/laravel
cp -fR $PWD/.temp/deployables/laravel $PWD
printf "Completed\n";
printf "_________\n";

echo "Updated On "$(date --utc +%FT%T.%3NZ)>> $PWD/updated.log
read -n1 -r -p "Update Complete, Please run Deploy. Press any key to Exit";




