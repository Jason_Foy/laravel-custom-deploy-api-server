#!/usr/bin/env bash

update(){
    echo "Running Update";
    echo "______________";

    if [ -d .temp ];
    then
      # Control will enter here if $DIRECTORY doesn't exist.
        echo "Remove Existing Files";
        rm -Rf .temp
    fi

    echo "Clone New Files";
    git clone https://mordicon@bitbucket.org/mordicon/laravel-basics.git .temp
    chmod 775 -R ./.temp/deployables/utilities
    ./.temp/deployables/utilities/update.sh
    exit;
}

deploy(){
    while true; do
        read -p "______________________
What would you like to deploy?

Build From Latest Update(b)?
Maintenance(m)?" response
        case $response in
            [b]* ) deploy=1; break;;
            [m]* ) deploy=2; break;;
            * ) echo "What would you like to deploy?

Build From Latest Update(b)?
Maintenance(m)?";;
        esac
    done

    if [ $deploy -eq 1 ];
    then
        ./utilities/build.sh
    fi

    if [ $deploy -eq 2 ];
    then
        ./utilities/maintenance.sh
    fi


}

#
#   Allow Other Scripts Use
#
chmod 775 -R ./.temp/deployables/utilities
chmod 775 -R ./utilities

#
#   PROMPT FOR ACTION
#
while true; do
    read -p "What would you like to do?

Update(u)?
Deploy(d)? :" response
    case $response in
        [d]* ) todo=1; break;;
        [u]* ) todo=2; break;;
        * ) echo "Please answer Update(u) or Deploy(d).";;
    esac
done

if [ $todo -eq 1 ];
    then
        deploy
    fi

if [ $todo -eq 2 ];
    then
        update
    fi



exit;




#Update Apache Configurations
#Setup Maintenance Site
a2dissite main
a2dissite maintenance
rm -Rf /etc/apache2/sites-available/main.conf
rm -Rf /etc/apache2/sites-available/maintenance.conf
cp /var/www/deployables/apache/maintenance.conf /etc/apache2/sites-available/
a2ensite maintenance

# Remove Old Site Files
rm -Rf /var/www/html


