@extends('layouts.internal')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Roles</div>
    <div class="panel-body">
        @if(!empty($roles))
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Active</th>
                        <th>Users</th>
                        <th>ACLS</th>
                    </tr>
                </thead>
            </table>
        </div>
            @foreach($roles as $role)

             {{$role}}

            @endforeach

        @endif
    </div>
</div>

@endsection