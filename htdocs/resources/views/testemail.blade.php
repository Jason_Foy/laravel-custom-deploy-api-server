@extends('layouts.app')

@section('scripts')
    <script src="/js/emailtest.js" type="text/javascript" rel="script"></script>
@endsection

@section('content')

    <div class="panel panel-default row">
        <div class="panel-body col-md-6 col-md-offset-3">

            <form action="/api/v1/email/upload" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}

                <div class="row form-group">
                    <div class="col-md-3">
                        Username
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="username"/>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3">
                        Password
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="password" name="passwd"/>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3">
                        Provider's Name
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="providers_name"/>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3">
                        Patient's Name
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="patients_name"/>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3">
                        Return E-mail Address
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="email"/>
                    </div>
                </div>

                <div class="row form-group">
                    <div class="col-md-3">
                        File Upload
                    </div>
                    <div class="col-md-9">
                        <input type="file" name="3dfile[]"/>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3">
                        Token
                    </div>
                    <div class="col-md-9">
                        <input class="form-control" type="text" name="token"/>
                    </div>
                </div>
                <input type="submit" value="GO!"/>
            </form>

        </div>
    </div>

@endsection