<?php
use Symfony\Component\DomCrawler\Form;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('landing');
Route::get('/logout',function(){
    Auth::logout();
    return redirect()->route('landing');
});

Auth::routes();

Route::get('/home', function (Request $request) {

//
//    // Store a piece of data in the session...
//    session(['key' => 'value']);
//
//    // Retrieve a piece of data from the session...
//    $value = session('key');
//
//    print_r($value);
    $controller = new \App\Http\Controllers\HomeController();
    return $controller->index();
});


Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin/roles', 'Users\RoleController@index');
});


Route::get('/test', function () {

    return view('test');
});

Route::get('/testemail', function () {

    return view('testemail');
});

//
//Route::get('upload', function () {
//    return view('testUpload');
//});
//
//
//Route::post('uploads', function () {
//    $file = request()->file('uploaded_file');
//
//
//    /**
//     * multiple Files Posted
//     */
//    if(is_array($file)){
//
//
//    }
//    echo "size:".$file->getSize();
//    echo "mime:".$file->getMimeType();
//
//    $test = $file->openFile()->fread($file->openFile()->getSize());
//
//    $repo = \App\Objects\Models\FileRepository::create([
//        'data'=>$test,
//        'extension_id'=>1
//    ]);
//
//});