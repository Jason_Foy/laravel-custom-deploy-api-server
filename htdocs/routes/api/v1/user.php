<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

Route::group([
    'middleware' => 'api',
    'prefix' => 'user',
], function () {

    /**
     * Login
     */
    Route::post('/login', function (Request $request) {
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password'), 'active' => 1], true)) {

            $token = (new \App\Objects\Components\TokenComponent())
                ->getTokenByUser(Auth::user());

            return [
                'result' => true,
                'token' => $token->getToken()
            ];
        }
        return ['result' => false, 'reason' => 'Unable to authenticate'];
    })->name('api/v1/login');

    /**
     * Manage User
     */
    Route::group([
        'middleware' =>'tokenAuth'
    ],function(){
        Route::post('manage', 'Api\V1\UserController@manage');
        Route::post('deactivate', 'Api\V1\UserController@disable');
        Route::get('list', 'Api\V1\UserController@getPatients');
    });
});