<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 1/10/2017
 * Time: 9:04 AM
 */

namespace App\Objects\Components;


use App\Objects\Models\User;
use App\Objects\Models\UserToken;

class TokenComponent
{

    private $token;

    /**
     * @return null
     */
    public function getToken()
    {
        if ($this->token == null)
            return null;
        return $this->token->token;
    }


    public function getTokenByUser(User $user)
    {
        $token = UserToken::where('active', 1)
            ->where('expires', '>', (new \DateTime('now'))->format('Y-m-d H:i:s'))
            ->where('user_id', $user->id)
            ->take(1)
            ->get();

        if (empty($token->first()))
            $this->token = $this->createToken($user);
        else
            $this->token = $token->first();

        return $this;
    }

    /**
     * @param $token
     * @return bool
     */
    public function getUserByToken($token){
        $token = UserToken::where('active', 1)
            ->where('expires', '>', (new \DateTime('now'))->format('Y-m-d H:i:s'))
            ->where('token', $token)
            ->take(1)
            ->get();
        if (empty($token->first()))
            return false;

        return $token->first()->user;
    }

    /**
     * @param User $user
     * @return static
     */
    private function createToken(User $user)
    {
        return UserToken::create([
            'user_id' => $user->id,
            'token' => str_random('90'),
            'active' => 1,
            'expires' => $this->tokenExpiration($user)
        ]);
    }

    /**
     * Create Token Expiration Based upon User Role
     * Client should have a longer login period and auto reset when used
     * @param User $user
     * @return Datetime
     */
    private function tokenExpiration(User $user)
    {
        return new \DateTime('tomorrow');
    }
}