<?php

/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 8:58 PM
 */
namespace App\Objects\Components;

use App\Objects\Models\User;
use App\Objects\Models\UserMeta;
use App\Objects\Models\UserMetaTypes;
use Illuminate\Support\Facades\Hash;


class UserComponent
{

    private $user;
    private $meta = [];

    /**
     * UserComponent constructor.
     * @param null $id
     */
    public function __construct($id = null)
    {
        $this->getById($id);
    }

    /**
     * Setup Component By User Id
     * @param $id
     * @return array
     */
    public function getById($id)
    {
        if (!empty($id))
            $user = User::find($id);
        else
            $user = new User();

        if (empty($user))
            return ['result' => false, 'response' => 'Unable to find user'];

        $this->user = $user;
        $this->getUserMeta();
    }

    /**
     * Update User data or Meta Data
     * @param array $userData
     */
    public function updateUserByArray(array $userData)
    {
        foreach ($userData as $key => $value) {
            if ($key == 'id')
                continue;
            /**
             * Update items from user table
             */
            if (in_array($key, $this->user->getFillable())) {
                if ($key == 'password' || $key == 'confirm_password')
                    $value = Hash::make($value);

                $this->user->$key = $value;
                continue;
            }

            /**
             * Check if value is Meta value
             */
            if (($metaType = $this->getMetaTypeByName($key)) == false)
                continue;

            /**
             * Check for update value
             */
            foreach ($this->meta as $meta) {
                if ($meta->meta_type_id == $metaType->id) {
                    $meta->type_value = $value;
                    continue 2;
                }
            }

            /**
             * Create New Meta Value
             */
            $meta = new UserMeta();
            $this->meta[] = $meta->fill([
                'type_value' => $value,
                'meta_type_id' => $metaType->id
            ]);
        }
    }

    /**
     * Save All Component Data
     */
    public function save()
    {
        /**
         * API added Users that do not have default CMS login rights
         */
        if (!isset($this->user->password) || empty($this->user->password)) {
            $this->user->password = 'd1on5ota4ll1ow3to7log8in7tot6hi4ss3y2stem';
            $this->user->active = 0;
        }

        $this->user->save();
        foreach ($this->meta as $meta) {
            $meta->user_id = $this->user->id;
            $meta->save();
        }

        return true;
    }

    /**
     * Deactivate User
     * @return bool
     */
    public function disable()
    {
        if (!isset($this->user->id))
            return false;

        $this->user->active = 0;
        return $this->user->save();
    }

    /**
     * Get Value in User or Meta
     * @param $attribute
     * @return bool
     */
    public function get($attribute)
    {
        /**
         * Search User
         */
        if (in_array($attribute, $this->user->getFillable())) {
            if (isset($this->user->$attribute))
                return $this->user->$attribute;
            return null;
        }elseif($attribute == 'created_at' || $attribute == 'modified_at')
            return $this->user->$attribute;
        /**
         * Search Meta
         */
        foreach ($this->meta as $meta) {
            if(($metaType = $this->getMetaTypeById($meta->meta_type_id))==false)
                continue;

            if ($attribute == $metaType->name)
                return $meta->type_value;
        }
        return null;
    }

    /**
     * Build Meta Collection
     */
    private function getUserMeta()
    {
        if (isset($this->user->id) && !empty($this->user->id)) {
            $meta = UserMeta::where('user_id', $this->user->id)->get();
            if (!empty($meta->all()))
                $meta = $meta->all();
        } else
            $meta = [];

        $this->meta = $meta;
    }

    /**
     * Get MetaType Id by Name
     * @param $name
     * @return mixed
     */
    private function getMetaTypeByName($name)
    {
        return $this->singleton(UserMetaTypes::where('name', $name)->get());
    }

    /**
     * @param $id
     * @return bool
     */
    private function getMetaTypeById($id)
    {
        $metaType = UserMetaTypes::find($id);
        if(empty($metaType))
            return false;
        return $metaType;
    }

    /**
     * @param $item
     * @return bool
     */
    private function singleton($item){
        if (!isset($item[0]))
            return false;
        return $item[0];
    }

}