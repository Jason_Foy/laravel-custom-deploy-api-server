<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/30/2016
 * Time: 12:51 AM
 */

namespace App\Objects\Models;


use Illuminate\Database\Eloquent\Model;

class FileRepository extends Model
{
    protected $table = 'file_repository';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'data','extension_id', 'updated_at'
    ];

}