<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //

    public function userRoles(){
        return $this->hasMany('App\Objects\UserRole');
    }
}
