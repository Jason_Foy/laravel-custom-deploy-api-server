<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class Entities extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id','entity_type_id', 'active','name','email'
    ];

    public function entityType(){
        return $this->hasOne('App\Objects\EntitiesTypes');
    }
}
