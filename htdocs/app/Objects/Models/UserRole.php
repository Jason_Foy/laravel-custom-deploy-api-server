<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    //
    public $timestamps = false;

    public function role(){
        return $this->belongsTo('App\Role','role_id','id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
