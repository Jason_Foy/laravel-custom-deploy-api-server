<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    protected $table = 'user_tokens';

    protected $fillable = [
        'user_id','token', 'active','expires'
    ];

    public function user(){
        return $this->belongsTo('App\Objects\Models\User','user_id');
    }
}
