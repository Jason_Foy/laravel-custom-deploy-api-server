<?php

namespace App\Objects\Models;

use Illuminate\Database\Eloquent\Model;

class EntitiesTypes extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'parent_id','entity_type_id', 'active'
    ];

}
