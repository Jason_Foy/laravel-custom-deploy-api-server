<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 1/9/2017
 * Time: 3:25 AM
 */

namespace App\Http\Middleware;

use App\Objects\Components\TokenComponent;
use Closure;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;

class TokenAuthentication
{
    /**
     * @param $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $tokenString = $request->input('token');
        if (strlen($tokenString) !== 90)
            return $this->failedAuthentication();

        if(($user = (new TokenComponent())->getUserByToken($tokenString))==false)
            return $this->failedAuthentication();

        Auth::login($user);
        return $next($request);
    }

    /**
     * @return mixed
     */
    private function failedAuthentication()
    {
        return response()->json(['result'=>false,'reason'=>'Unable to Authenticate Token','code'=>403]);
    }

}