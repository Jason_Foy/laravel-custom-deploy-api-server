<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 1/9/2017
 * Time: 3:23 AM
 */

namespace App\Http\Controllers\Api\V1;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class PracticeController extends Controller
{

    public function providersByPractice(){

        $pId = Request::input('practiceId');
        if(empty($pId) || intval($pId) == 0 )
            return ['result'=>false,'reason'=>'practiceId required'];

        $providers =  \App\Objects\Models\Entities::whereRaw('( entity_type_id = ? OR entity_type_id = ?) and active = ?',[5,6,1])
            ->get();

        return $providers;

    }
}