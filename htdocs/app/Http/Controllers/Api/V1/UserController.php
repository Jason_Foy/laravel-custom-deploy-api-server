<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/20/2016
 * Time: 8:04 PM
 */

namespace App\Http\Controllers\Api\V1;

use App\Objects\Components\UserComponent;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Add or update a user
     * @param Request $request
     * @return array
     */
    public function manage(Request $request){
        $user = new UserComponent($request->id);
        $user->updateUserByArray($request->all());
        return ['result'=>$user->save()];
    }

    /**
     * Deactivate User
     * @param Request $request
     * @return array
     */
    public function disable(Request $request){
        $user = new UserComponent($request->id);
        return ['result'=>$user->disable()];
    }

    /**
     * TODO: Rebuild this, it's built quick and dirty for temp functions
     */
    public function getPatients(){
        $userList =  \App\Objects\Models\User::where('id','!=',1)->get();

        $users = [];
        foreach($userList as $user){
            $users[] = new UserComponent($user->id);
        }

        $returnData = [];
        foreach($users as $user){
            $returnData[] = [
                'name'=>$user->get('first_name').' '.$user->get('last_name'),
                'weight'=>$user->get('weight'),
                'dob'=>$user->get('dob'),
                'email'=>$user->get('email'),
                'created_at'=>$user->get('created_at')
            ];
        }
        return $returnData;
    }
}