<?php
/**
 * Created by PhpStorm.
 * User: Jason Foy
 * Date: 12/14/2016
 * Time: 12:15 PM
 */

namespace App\Http\Controllers\Users;


use App\Http\Controllers\Controller;
use App\Role as Role;
use App\UserRole;

use App\Factories\AdminUsersFactory;

class RoleController extends Controller
{
    public function index()
    {
        $roles = [];

        $AUF = new AdminUsersFactory();
        $AUF->getAdminRoles();

        return view('admin/users/role', ['roles' => $roles]);
    }

}