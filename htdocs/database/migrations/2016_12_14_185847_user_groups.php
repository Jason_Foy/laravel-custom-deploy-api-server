<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserGroups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
            $table->boolean('active');
        });

        Schema::create('user_roles', function (Blueprint $t) {
           $t->increments('id');
           $t->unsignedInteger('user_id');
           $t->unsignedInteger('role_id');
           $t->foreign('user_id')->references('id')->on('users');
           $t->foreign('role_id')->references('id')->on('roles');
        });

        Schema::create('acls', function(Blueprint $t){
           $t->increments('id');
           $t->string('controller');
           $t->string('action');
        });

        Schema::create('role_acls', function(Blueprint $t){
            $t->increments('id');
            $t->unsignedInteger('role_id');
            $t->unsignedInteger('acl_id');

            $t->foreign('role_id')->references('id')->on('roles');
            $t->foreign('acl_id')->references('id')->on('acls');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('role_acls');
        Schema::drop('acls');
        Schema::drop('user_roles');
        Schema::drop('roles');
    }
}
