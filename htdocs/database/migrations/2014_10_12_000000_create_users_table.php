<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * USERS
         */
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('password');
            $table->boolean('active')->default('1');
            $table->rememberToken();
            $table->timestamps();
        });

        /**
         * User Meta Types
         */
        Schema::create('user_meta_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });

        /**
         * Default Meta
         */
        DB::table('user_meta_types')->insert([
            [
                'name' => 'Last Login',
                'description' => 'User Last Login Date'
            ],
        ]);

        Schema::create('user_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('meta_type_id');
            $table->string('type_value');
            $table->timestamps();

            /**
             * Unique type by user
             */
            $table->unique(['user_id', 'meta_type_id']);

            /**
             * FK
             */
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('meta_type_id')->references('id')->on('user_meta_types');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_meta');
        Schema::drop('user_meta_types');
        Schema::drop('users');
    }
}
