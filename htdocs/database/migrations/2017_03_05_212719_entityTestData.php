<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EntityTestData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('practices')->insert(
            [
                [
                    'name'=>'Marvin\'s Fix It House',
                    'account_manager_phone'=>'8015553245',
                    'account_manager_email'=>'leo.marvin@fixit.com',
                    'account_manager_first_name'=>'Leo',
                    'account_manager_last_name'=>'Marvin',
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ],
                [
                    'name'=>'Acula\'s Practice',
                    'account_manager_phone'=>'8015553245',
                    'account_manager_email'=>'Dr.Acula@acula.com',
                    'account_manager_first_name'=>'Acula',
                    'account_manager_last_name'=>'Bloodworth',
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ]
            ]
        );

        DB::table('clinics')->insert(
            [
                [
                    'practice_id'=>1,
                    'name'=>'Bob\'s House',
                    'address'=>'1234 North Street Drive',
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ],
                [
                    'practice_id'=>2,
                    'name'=>'Horror House',
                    'address'=>'1234 North Street Drive',
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ]
            ]
        );

        DB::table('providers')->insert(
            [
                [
                    'clinic_id'=>1,
                    'first_name'=>'Leo',
                    'last_name'=>'Marvin',
                    'is_orthotist'=>0,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ],
                [
                    'clinic_id'=>2,
                    'first_name'=>'Acula',
                    'last_name'=>'Bloodworth',
                    'is_orthotist'=>1,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ]
            ]
        );

        $date = (new DateTime('now'))->modify('-12 years')->format('Y-m-d H:i:s');


        DB::table('patients')->insert(
            [
                [
                    'provider_id'=>1,
                    'first_name'=>'Bob',
                    'last_name'=>'Willey',
                    'weight'=>126.4,
                    'dob'=>$date,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ],
                [
                    'provider_id'=>1,
                    'first_name'=>'Junior',
                    'last_name'=>'Holdin',
                    'weight'=>103,
                    'dob'=>$date,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ],
                [
                    'provider_id'=>2,
                    'first_name'=>'Johnny',
                    'last_name'=>'Homicidal',
                    'weight'=>126.4,
                    'dob'=>$date,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ],
                [
                    'provider_id'=>2,
                    'first_name'=>'King',
                    'last_name'=>'Leoric',
                    'weight'=>126.4,
                    'dob'=>$date,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ],
                [
                    'provider_id'=>2,
                    'first_name'=>'Albia',
                    'last_name'=>'Da\'mad',
                    'weight'=>126.4,
                    'dob'=>$date,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ]
            ]
        );

        DB::table('users')->insert(
            [
                [
                    'username'=>'leo.marvin.practice',
                    'email'=>'leo.marvin.practice@fixit.com',
                    'password'=>'$2y$10$aMLi5A99n86jLtbRaZRVxeaHcwXYc2I9eEuKjWhr5YZzUBcNflMXa',
                    'active'=>1,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ],
                [
                    'username'=>'dr.acula.practice',
                    'email'=>'dr.acula.practice@acula.com',
                    'password'=>'$2y$10$aMLi5A99n86jLtbRaZRVxeaHcwXYc2I9eEuKjWhr5YZzUBcNflMXa',
                    'active'=>1,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ],
                [
                    'username'=>'leo.marvin',
                    'email'=>'leo.marvin@fixit.com',
                    'password'=>'$2y$10$aMLi5A99n86jLtbRaZRVxeaHcwXYc2I9eEuKjWhr5YZzUBcNflMXa',
                    'active'=>1,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ],
                [
                    'username'=>'leo.marvin',
                    'email'=>'leo.marvin@fixit.com',
                    'password'=>'$2y$10$aMLi5A99n86jLtbRaZRVxeaHcwXYc2I9eEuKjWhr5YZzUBcNflMXa',
                    'active'=>1,
                    'created_at'=>gmdate('Y-m-d H:i:s'),
                    'updated_at'=>gmdate('Y-m-d H:i:s')
                ]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        //
    }
}
