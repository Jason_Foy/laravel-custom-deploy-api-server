<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScanAndStorage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        /**
         * Extensions
         */
        Schema::create('file_repository_extensions',function(Blueprint $table){
           $table->increments('id');
           $table->string('extension');
           $table->string('mime');
           $table->string('description');
           $table->timestamps();

           $table->unique('extension','mime');
        });

        DB::table('file_repository_extensions')->insert([
            [
                'extension'=>'jpg',
                'mime'=> 'image/jpeg',
                'description'=>'jpeg File'
            ],
            [
                'extension'=>'jpeg',
                'mime'=> 'image/jpeg',
                'description'=>'jpeg file'
            ],
            [
                'extension'=>'zip',
                'mime'=> 'application/zip',
                'description'=>'zip compressed file'
            ],
            [
                'extension'=>'rar',
                'mime'=> 'application/x-rar-compressed',
                'description'=>'rar compressed file'
            ]
        ]);

        /**
         * File Repository For all Files In the System
         */
        Schema::create('file_repository', function (Blueprint $table) {
            $table->uuid('uuid')->primary();
            $table->unsignedInteger('extension_id');
            $table->binary('data');
            $table->timestamps();

            $table->foreign('extension_id')->references('id')->on('file_repository_extensions');
        });

        /**
         * User Scans
         * Scan for a patient by a doctor
         */
        Schema::create('scans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('owner_user_id');
            $table->unsignedInteger('provider_user_id');
            $table->timestamps();

            $table->foreign('owner_user_id')->references('id')->on('users');
            $table->foreign('provider_user_id')->references('id')->on('users');
        });


        /**
         * Scan Files
         * Each scan will have 2-4 files associated with it for Unity/Product and Right/Left
         */
        Schema::create('scan_files', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('scan_id');
            $table->uuid('file_repository_id');
            $table->enum('orientation', ['left', 'right']);
            $table->enum('environment', ['object', 'product']);
            $table->timestamps();

            $table->unique(['scan_id', 'orientation', 'environment']);
            $table->foreign('scan_id')->references('id')->on('scans');
            $table->foreign('file_repository_id')->references('uuid')->on('file_repository');
        });

        DB::unprepared('
        CREATE TRIGGER init_uuid_file_repository BEFORE INSERT  ON file_repository
        FOR EACH
        ROW  SET NEW.uuid = UUID(  ) ;
        ');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('scan_file_meta');
        Schema::drop('scan_files');
        Schema::drop('file_repository');
        Schema::drop('file_repository_extensions');
        Schema::drop('scans');
    }
}
