<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScanFileMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

        /**
         * TODO: Remove this entire table and build the real system on phase 2
         */
        Schema::create('scan_file_meta', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('scan_file_id');
            $table->string('meta_key');
            $table->string('meta_value');
            $table->timestamps();

            $table->foreign('scan_file_id')->references('id')->on('scan_files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //


    }
}
